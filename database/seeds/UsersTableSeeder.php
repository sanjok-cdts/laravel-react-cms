<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'name' => 'Admin',
            'email' => 'admin@conndots.test',
            'password' => Hash::make('admin123'),
            'isAdmin' => '1',
        ],
        [
            'name' => 'Admin2',
            'email' => 'admin2@conndots.test',
            'password' => Hash::make('admin123'),
            'isAdmin' => '1',
        ],
        [
            'name' => 'user',
            'email' => 'user@conndots.test',
            'password' => Hash::make('user123'),
            'isAdmin' => '0',
        ]
        ]
    );
    }
}
