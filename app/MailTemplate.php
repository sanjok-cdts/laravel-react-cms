<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Http\Request;

class MailTemplate extends Model
{
    use SoftDeletes;
    protected $fillable=['id','title','comment'];

    public function addMailTemplate($request){
        $data=$request->except('_token');
        $this->fill($data);
        return $this->save();
    }

    public static function getCommentData(){
        
        $value=DB::table('mail_templates')->orderBy('id', 'asc')->get(); 
        
        return $value;
      
      }
}
