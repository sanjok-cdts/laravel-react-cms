<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    protected $table = "employees";
    protected $fillable=['id','name','job_title','role','position','department','employee_image','short_detail'];
}
