<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use SoftDeletes;
    protected $fillable=['id','user_id','name','employee_id','position','age','Address'];
    // public function getAddRules(){
    //     return[
    //         'employee_id'=>'required|integer',
    //         'position'=>'required|string',
    //         'age'=>'required|integer',
            
    //     ];
    // }
    public function addUserDetails($request){
        $data=$request->except('_token');
        $this->fill($data);
        return $this->save();
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    
    

}
