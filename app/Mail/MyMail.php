<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $comment;
    public $title;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($comment, $title)
    {
        //dd($title);
        $this->title = $title;
        $this->comment =  $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('Admin@conndots.test')
        ->subject($this->title)
        ->view('mail.mymail');  
    }
}
