<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MailTemplate;

class AjaxController extends Controller {
   public function index() {
      
      $msg = "This is a simple message.";
      //return response()->json(['name' => 'Abigail', 'state' => 'CA']);
      return response()->json(array('msg'=> $msg), 200);
   }
   public function getComment(){
    //$msg = "This is a Success message.";
    //return response()->json(['name' => 'Abigail', 'state' => 'CA']);
    //return response()->json(array('msg'=> $msg), 200);

    $comment['data'] = MailTemplate::getCommentData();
    
    echo json_encode($comment);
    exit;

   }
}