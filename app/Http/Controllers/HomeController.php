<?php

namespace App\Http\Controllers;
use App\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use DB;
use App\User;
use App\MailTemplate;

class HomeController extends Controller
{
    protected $user_details = null;
  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(UserDetails $user_detail)
    {
       // $data = DB::table('user_details')->get();
        $data = UserDetails::get();
       
        return view('home')->with('user_data', $data);
    }
    public function Mailindexblank(){
        return view('backend.SendMail')->with('email_id',  "" );

    }
    public function Mailindex(Request $request,MailTemplate $mailtemplate)
    {
        $uid = $request->id;
        $user_id = UserDetails::where('id', $uid)->first();
        //dd($user_id);
        $user = User::where('id', $user_id->user_id)->first();
        $required_email = $user->email;

        $data =  MailTemplate::get();
    

        return view('backend.SendMail')
        ->with('required_data',  $data )
        ->with('email_id',  $required_email );
  
    }

    public function MailSent(Request $request, Mailer $mailer) 
    {
        $mailer
        ->to($request->input('mail'))
        
        ->send(new \App\Mail\MyMail($request->input('comment'), $request->input('title')));

        return redirect()->back();
    }

   
    public function create()
    {
        return view('frontend.create');
    }
    public function TestIndex(){
        return view('backend.test');
    }
   
}
