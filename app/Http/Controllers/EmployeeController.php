<?php

namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public $employees = null;
    public function __construct(Employee $employee)
    {
        $this->employees = $employee;
    }

    public function index(){

        return view('backend.employeeForm');
    }
    public function submitEmployeeForm(Request $request){
        $employees = new Employee;

        $employees->name = $request->input('name');
        $employees->job_title = $request->input('job_title');
        $employees->role = $request->input('role');
        $employees->position = $request->input('position');
        $employees->department = $request->input('department');
        $employees->employee_image = $request->input('employee_image');
        $employees->short_detail = $request->input('short_detail');

        $employees->save();

    }

    public function emplyeeIndex(){
        $data = $this->employees->orderBy('position', 'asc')->orderBy('updated_at', 'asc')->get();
        
        return view('backend.employeeIndex')->with('employee_data', $data);
    }
    public function DragIndex(){
        $data = $this->employees->orderBy('position', 'asc')->orderBy('updated_at', 'asc')->get();
        
        return view('backend.dragIndex')->with('employee_data', $data);
    }
    public function SubmitDrag(Request $request){
        $employees = new Employee;

        $required_positions =  $request->get('positions');
        foreach($required_positions as $value){
            $required_id = $value[0];
            $required_position = $value[1];
        }
        //return response()->json($required_position);
         $employees->where('id', $required_id)->update(['position'=>$required_position]);

         $employees->update();

      
    }
    public function DeleteDrag(Request $request){
        $employees = new Employee;
        $required_id =  $request->id;

        // if($_GET['id']){
        //     $id=$_GET['id'];
        //     $id = mysql_escape_string($id);
        // }
        $employees->where('id', $required_id)->delete();
        
        // $result = mysql_query($del);
    }

    public function EmployeeindexApi(){
        return Employee::all();
    }
    public function showApi($id){
        return Employee::find($id);
       
    }
    public function storeApi(Request $request){
        //dd($request->all());
        $employee = Employee::create($request->all());

        return response()->json( $employee, 201);
    }
    public function updateApi(Request $request, $id){
        //dd($id);
        //dd($request->input());
        $employee = Employee::findOrFail($id);

        $employee->update($request->all());

        return response()->json($employee);

    }

    public function deleteApi($id){

         Employee::find($id)->delete();
       

        return 204;
    }
}
