<?php

namespace App\Http\Controllers;

use App\MailTemplate;
use Illuminate\Http\Request;

class MailTemplateController extends Controller
{
    protected $mailtemplates = null;
    public function __construct(MailTemplate $mailtemplate)
    {
        $this->mailtemplates = $mailtemplate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->mailtemplates->get();
       
        return view('backend.mailtemplate.template')->with('template_data', $data);
    }

    public function TemplateSent(Request $request){
        $this->validate($request, [
            'title' => 'required|string',
            'comment' => 'required|string',
        ]);
        $success = $this->mailtemplates->addMailTemplate($request);
        if ($success) {

            $request->session()->flash('success', 'Mail Template added successfully');
        } else {
            $request->session()->flash('sorry', 'Mail Template could not added');
        }
        return redirect()->route('index-mail-template');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(MailTemplate $mailTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(MailTemplate $mailTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MailTemplate $mailTemplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(MailTemplate $mailTemplate)
    {
        //
    }
}
