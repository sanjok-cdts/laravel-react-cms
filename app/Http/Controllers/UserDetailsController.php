<?php

namespace App\Http\Controllers;

///this is test for git commit 
use App\UserDetails;
use Redirect;

use Illuminate\Http\Request;

class UserDetailsController extends Controller
{
    protected $user_details = null;
    public function __construct(UserDetails $user_detail)
    {
        $this->user_details = $user_detail;
    }

    public function detailsubmit(Request $request)
    {
        // dd($request);


        // $act="add";

        //$rules=$this->user_details->getAddRules();

        $this->validate($request, [
            'employee_id' => 'required|integer',
            'position' => 'required|string',
            'age' => 'required|integer',
        ]);

        // if($validator->fails()) {
        //     return redirect()->back()->with($validator);
        // }
        $success = $this->user_details->addUserDetails($request);
        if ($success) {

            $request->session()->flash('success', 'User Details added successfully');
        } else {
            $request->session()->flash('sorry', 'User Details could not added');
        }
        return redirect()->route('home');
    }
    public function AdminIndex()
    {
        $data = $this->user_details->get();

        return view('backend.users-detail')->with('user_data', $data);
    }
    public function getUserForm(Request $request)
    {
        $act = ($request->id == 'post') ? 'add' : 'update';
        if ($act !== 'add') {
            $user_detail = $this->user_details->find($request->id);

            if (!$user_detail) {
                $request->session()->flash('success', 'USER not found');
                return redirect()->route('users-data');
            }
        }


        return view('backend.user-form')
            ->with('title', $act)
            ->with('user_data', $user_detail);
    }

    public function submitUserForm(Request $request)
    {
        $act = "add";

        if (isset($request->id) && $request->id != null) {
            $act = "updat";
            $this->user_details = $this->user_details->find($request->id);
        }

        $this->validate($request, [
            'employee_id' => 'required|integer',
            'position' => 'required|string',
            'age' => 'required|integer',
        ]);


        $success = $this->user_details->addUserDetails($request);
        if ($success) {
            $request->session()->flash('success', 'Updateded successfully');
        } else {
            $request->session()->flash('sorry', ' could not be Updated');
        }
        return redirect()->route('user-details');
    }
    public function deleteUserData(Request $request){
        $user_detail  =  $this->user_details->find($request->id);

        if(!$user_detail)
        {
            $request->session()->flash('error','USER not Found');
            return redirect()->route('user-details');
            
        }

        $del = $user_detail->delete();
        return redirect()->route('user-details');


    }
}
