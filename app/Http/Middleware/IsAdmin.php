<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()){

        
        if($request->user()->isAdmin=='1'){
            return $next($request);
            } else{
                return redirect()-> route('home');
            }
        }
   
    else{
        return redirect()-> route('home');
    }
}
}