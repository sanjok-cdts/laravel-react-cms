<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\EmployeeController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/filluptheform', 'HomeController@create')->name('fillform');


Route::post('/userdetails-submit', 'UserDetailsController@detailsubmit')->name('detail-submit');

// Route::group(['prefix'=>'/users'], function(){
//     Route::get('/','UserDetailsController@AdminIndex')->name('user-details');
//     Route::get('/{id}','UserDetailsController@getUserForm')->name('user-edit');
//     Route::post('/post','UserDetailsController@submitUserForm')->name('user-submit');
//     Route::get('/delete/{id}','UserDetailsController@deleteUserData')->name('user-deleteData');
// });

// Route::get('/admin', ['middleware' => 'admin', function () {
//     return view('backend.admin');
// }]);
Route::get('ajax',function() {
    return view('backend.message');
 });
Route::post('/getmsg','AjaxController@index');

Route::group(['prefix'=>'/admin','middleware'=>['admin']],function(){
    Route::any('/',function(){
        return view ('backend.admin');
    })->name('admin');

    Route::get('/test','HomeController@TestIndex')->name('test');

    Route::group(['prefix'=>'/users'], function(){
            Route::get('/','UserDetailsController@AdminIndex')->name('user-details');
            Route::get('/{id}','UserDetailsController@getUserForm')->name('user-edit');
            Route::post('/post','UserDetailsController@submitUserForm')->name('user-submit');
            Route::get('/delete/{id}','UserDetailsController@deleteUserData')->name('user-deleteData');
        });

        Route::post('/getcomment','AjaxController@getComment');
        
        Route::get('/employeeform','EmployeeController@index');
        Route::get('/test-drag','EmployeeController@Dragindex');
        Route::post('/test-post','EmployeeController@SubmitDrag');
        Route::get('/test-delete','EmployeeController@DeleteDrag');
        
        Route::get('/employeeDetails','EmployeeController@emplyeeIndex')->name('employee-index');
        Route::post('/employee-add','EmployeeController@submitEmployeeForm')->name('employee-submit');
      
     
        Route::group(['prefix'=>'/sendmail'], function(){   
             Route::any('/', 'HomeController@Mailindexblank')->name('mail-home-blank');
             Route::get('/{id}', 'HomeController@Mailindex')->name('sendmail');
            Route::post('/', 'HomeController@MailSent')->name('sendmails');
        });

        Route::group(['prefix'=>'/mailtemplate'], function(){   
            Route::any('/', 'MailTemplateController@index')->name('index-mail-template');
            
           Route::post('/', 'MailTemplateController@TemplateSent')->name('add-mail-template');
       });
        //Route::any('/sendmail', 'HomeController@Mailindexblank')->name('mail-home-blank');
        //Route::get('/sendmail/{id}', 'HomeController@Mailindex')->name('sendmail');
});
