<?php

use Illuminate\Http\Request;
Use App\Employee;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('employee', 'EmployeeController@EmployeeindexApi');
Route::get('employee/{id}', 'EmployeeController@showApi');
Route::post('employee', 'EmployeeController@storeApi');
Route::put('employee/{id}', 'EmployeeController@updateApi');
Route::delete('employee/{id}', 'EmployeeController@deleteApi');