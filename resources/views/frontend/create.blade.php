@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ADD DETAILS</div>
                
                <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {{ Form::open(['route' =>'detail-submit', 'method'=>'POST']) }}
                        <div class="form-group row">
                            <label for="" class="col-sm-3">Employee ID:</label>
                            <div class="col-sm-8">
                                {{ Form::text('employee_id', null, ['class'=>'form-control','required'=>'true',])}}
                                
                                @if($errors->has('employee_id'))
                                <span class="help-block"> {{ $errors->first('employee_id') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">POSITION:</label>
                            <div class="col-sm-8">
                                {{ Form::text('position', null, ['class'=>'form-control','required'=>'true',])}}
                                 @if($errors->has('position'))
                                <span class="help-block"> {{ $errors->first('position') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">Age:</label>
                            <div class="col-sm-8">
                                {{ Form::text('age', null, ['class'=>'form-control','required'=>'true',])}}
                                 @if($errors->has('age'))
                                <span class="help-block"> {{ $errors->first('age') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">Address:</label>
                            <div class="col-sm-8">
                                {{ Form::text('Address', null, ['class'=>'form-control','required'=>'true',])}}
                                 @if($errors->has('Address'))
                                <span class="help-block"> {{ $errors->first('Address') }}</span>

                                @endif 
                            </div>
                            <div class="col-sm-8">
                          {{Form::button('submit',  ['class'=>'btn btn-success', 'type' => 'submit'])}}
                          </div>

                        </div>
                        {{ Form::hidden('user_id', Auth::user()->id )}}
                        {{ Form::hidden('name', Auth::user()->name) }}
                        {{ Form::close() }}

                
            </div>
        </div>
    </div>
</div>
@endsection