@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8 ">
        <div class="row justify-content-md-center">
            @foreach($employee_data as $value)
            @if($value->role == 0)
            <div class="col-md-3 ">
            <div class="card text-center" style="width: 18rem;">

                <img class="card-img-top" style="height: 200px" src="https://source.unsplash.com/random" alt="Card image cap">
                <div class="card-body">
                    <h3 class="card-title" style="font-weight: bolder;">{{$value->name}}</h3>
                    <h4 class="card-title">{{$value->department}}</h4>
                    <h5 class="card-title">{{$value->job_title}}</h5>
                    <p class="card-text">{{$value->short_detail}}</p>
                    
                </div>
            </div>
            </div>
            @endif
            @endforeach

        </div>

        <div style="margin:70px 0px 70px 0px" class="row justify-content-between">
            @foreach($employee_data as $value)
            @if($value->role == 1)
            <div class="card" style="width: 18rem; margin:10px 0px;">

                <img class="card-img-top" style="height: 200px" src="https://source.unsplash.com/random" alt="Card image cap">
                <div class="card-body">
                <h3 class="card-title" style="font-weight: bolder;">{{$value->name}}</h3>
                    <h4 class="card-title">{{$value->department}}</h4>
                    <h5 class="card-title">{{$value->job_title}}</h5>
                    <p class="card-text">{{$value->short_detail}}</p>
                    
                </div>
            </div>
            @endif
            @endforeach
        </div>

        <div style="margin:70px 0px 70px 0px"class="row justify-content-between">
            @foreach($employee_data as $value)
            @if($value->role == 2)
            <div class="card" style="width: 18rem; margin:10px 0px;">

                <img class="card-img-top" style="height: 200px" src="https://source.unsplash.com/random" alt="Card image cap">
                <div class="card-body">
                <h3 class="card-title" style="font-weight: bolder;">{{$value->name}}</h3>
                    <h4 class="card-title">{{$value->department}}</h4>
                    <h5 class="card-title">{{$value->job_title}}</h5>
                    <p class="card-text">{{$value->short_detail}}</p>
                    
                </div>
            </div>
            @endif
            @endforeach
        </div>

    </div>
</div>

@endsection