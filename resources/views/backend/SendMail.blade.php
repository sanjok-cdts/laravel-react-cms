@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header " style="font-size:20px; font-weight:400; ">SEND MAIL
                <form method="post" action="{{route('sendmails')}}">  
                    <div class="form-group">
                    
                        E-mail: <input type="text" name="mail" class="form-control" value="{{($email_id) }}">
                        <span class="error"> </span>
                        
                    </div>
                    <div class="form-group">
                       <!-- Title: <input type="text" name="title" class="form-control">
                        <br><br> -->
                        <label for="sel1"> Title:</label>
                        <select class="form-control" name="title" id="title_of_mail">
                            @foreach($required_data as $value)
                            <option>{{$value->title}}</option>
                            @endforeach
                        </select>
                    
                    </div>  
                    <div class="form-group">
                        Comment: <textarea name="comment" rows="5" class="form-control" id="comment"cols="40"></textarea>
                        <br><br>
                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{ csrf_field() }}
                    </form>
                </div>
               
                
            </div>
        </div>
    </div>
</div>
@endsection


<script type="text/javascript">
     window.onload=function() { // when the page has loaded
      document.getElementById("title_of_mail").onchange=function() {
        var val = this.value;
        //console.log(val);
       
            //  console.log("HERE");
            $.ajax({
               type:'POST',
               url:'/admin/getcomment',
               data: {
                "_token": "{{ csrf_token() }}",
                // "title" : val,
               
                },
                error: function(xhr, status, error) {
                  
                  var err = eval("(" + xhr.responseText + ")");
                 console.log(err);
                  },
               success:function(data) {
                   //console.log("HERE");
                 // $("#comment").html(data.msg);
                 var jsonObj = JSON.parse(data);
                 //console.log(jsonObj.data);
                 var JObject = jsonObj.data;
                 for(var i = 0; i < JObject.length; i++)
                 {
                    if(val == JObject[i].title){
                        //console.log(JObject[i].comment);
                        $("#comment").html(JObject[i].comment);
                    }
                }
               }

            });
         
      }
    }

         

</script>