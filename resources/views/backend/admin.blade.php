@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                @endif
  
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">USERS SECTION</h5>
                      
                        <a href="{{ route('user-details')}}">SEE USER DETAILS</a>
                    </div>
                    </div>

                    <div class="card " style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">TASK ASSIGNMENT</h5>
                        
                        <a href="{{ route('user-details')}}">ASSIGN TASK TO USER</a>
                    </div>
                    </div>

                    <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">MAIL TEMPLATE</h5>
                       
                        <a href="{{ route('index-mail-template')}}">ADD MAIL TEMPLATE</a>
                    </div>
                    </div>

                    <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title" style="color:salmon">EMPLOYEE SECTION</h5>
                       
                        <a href="{{ route('employee-index')}}">SEE EMPLOYEES</a>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
