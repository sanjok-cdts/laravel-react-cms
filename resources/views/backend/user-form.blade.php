@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ ucfirst($title) }} DETAILS</div>
                
                <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {{ Form::open(['route' =>'user-submit', 'method'=>'POST']) }}
                        <div class="form-group row">
                            <label for="" class="col-sm-3">Employee ID:</label>
                            <div class="col-sm-8">
                                
                                {{ Form::text('employee_id',@$user_data->employee_id, ['class'=>'form-control','required'=>'true',])}}
                                
                                @if($errors->has('employee_id'))
                                <span class="help-block"> {{ $errors->first('employee_id') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">Name:</label>
                            <div class="col-sm-8">
                                
                                {{ Form::text('name',@$user_data->name, ['class'=>'form-control','required'=>'true',])}}
                                
                                @if($errors->has('name'))
                                <span class="help-block"> {{ $errors->first('name') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">POSITION:</label>
                            <div class="col-sm-8">
                                {{ Form::text('position', @$user_data->position, ['class'=>'form-control','required'=>'true',])}}
                                 @if($errors->has('position'))
                                <span class="help-block"> {{ $errors->first('position') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">Age:</label>
                            <div class="col-sm-8">
                                {{ Form::text('age', @$user_data->age, ['class'=>'form-control','required'=>'true',])}}
                                 @if($errors->has('age'))
                                <span class="help-block"> {{ $errors->first('age') }}</span>

                                @endif 
                            </div>
                            <label for="" class="col-sm-3">Address:</label>
                            <div class="col-sm-8">
                                {{ Form::text('Address', @$user_data->Address, ['class'=>'form-control','required'=>'true',])}}
                                 @if($errors->has('Address'))
                                <span class="help-block"> {{ $errors->first('Address') }}</span>

                                @endif 
                            </div>
                            <div class="col-sm-8">
                          {{Form::button('submit',  ['class'=>'btn btn-success', 'type' => 'submit'])}}
                          </div>

                        </div>
                        {{ Form::hidden('user_id', @$user_data->user_id )}}
                        {{ Form::hidden('id', @$user_data->id )}}
                        
                        {{ Form::close() }}

                
            </div>
        </div>
    </div>
</div>
@endsection