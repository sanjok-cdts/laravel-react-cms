<html>
   <head>
      <meta content = "text/html; charset = ISO-8859-1" http-equiv = "content-type">
	
	
      <title>tutorialspoint.com JSON</title>
   </head>
	
   <body>
      <h1>Cricketer Details</h1>
		
      <table class = "src">
         <tr><th>Message</th><th>Color</th></tr>
         <tr><td><div id = "message">This is a message</div></td>
         <td><div id = "color">Color</div></td></tr>
      </table>

      <div class = "central">
         <button type = "button" onclick = "loadJSON()">Update Details </button>
      </div>
        
      	
      <script type = "application/javascript">
         function loadJSON() {
            var data_file = "https://happycoding.io/tutorials/javascript/example-ajax-files/random-welcomes.json";
            var http_request = new XMLHttpRequest();
            try{
               // Opera 8.0+, Firefox, Chrome, Safari
               http_request = new XMLHttpRequest();
            }catch (e) {
               // Internet Explorer Browsers
               try{
                  http_request = new ActiveXObject("Msxml2.XMLHTTP");
					
               }catch (e) {
				
                  try{
                     http_request = new ActiveXObject("Microsoft.XMLHTTP");
                  }catch (e) {
                     // Something went wrong
                     alert("Your browser broke!");
                     return false;
                  }
					
               }
            }
			
            http_request.onreadystatechange = function() {
			
               if (http_request.readyState == 4  ) {
                  // Javascript function JSON.parse to parse JSON data
                  
                  var jsonObj = JSON.parse(http_request.responseText);
                  console.log(jsonObj.randomMessages);
                  for(var i = 0; i < jsonObj.randomMessages.length; i++){
                    //console.log(jsonObj.randomMessages[i].message);
                    document.getElementById("message").innerHTML += '<ul>'+
					'<li>'+jsonObj.randomMessages[i].message+ '</li>'+
					'</ul>';

                    document.getElementById("color").innerHTML +='<ul>'+
					'<li>'+jsonObj.randomMessages[i].color+ '</li>'+
					'</ul>';
                  }
                //   var values = Object.values(jsonObj); 
                //   for(var i = 0; i < jsonObj.length; i++)
                //   {
                //     for(var j = 0; j < jsonObj.length; j++)
                //     {
                //         document.getElementById("message").innerHTML = values[i][j].message ;
                //          document.getElementById("color").innerHTML =values[i][j].color;
                //         //console.log(values[i][j].message);
                //     }
                //   }
                //   alert(jsonObj);
                //   console.log(JSON.stringify(jsonObj));
                 
                  // jsonObj variable now contains the data structure and can
                  // be accessed as jsonObj.name and jsonObj.country.


                //   document.getElementById("messge").innerHTML = jsonObj.message ;
                //   document.getElementById("color").innerHTML = jsonObj.color;
              

               }
            }
			
            http_request.open("GET", data_file, true);
            http_request.send();
         }
		
      </script>
   </body>
		
</html>