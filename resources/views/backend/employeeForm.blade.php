@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!-- Bootstrap Modal  -->
            
                    

                    <!-- Modal -->
                    <div class="modal fade" id="add-employee-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <form id = "addform">

                        {{ csrf_field() }}
                            <div class="form-group">
                                <label >Name</label>
                                <input type="text" class="form-control" name = "name" id="name"   placeholder="Enter Employee Name">
                            </div>
                            <div class="form-group">
                                <label>Job Title</label>
                                <input type="text" class="form-control" name = "job_title" id="job_title"   placeholder="Enter your Job Title">
                             </div>
                             <div class="form-group">
                                <label>Role</label>
                                <input type="text" class="form-control" name = "role" id="role"   placeholder="Role">
                             </div>
                             <div class="form-group">
                                <label>Position</label>
                                <input type="text" class="form-control" name = "position" id="position"   placeholder="">
                             </div>
                             <div class="form-group">
                                <label>Department</label>
                                <input type="text" class="form-control" name = "department" id="department"   placeholder="">
                             </div>
                             <div class="form-group">
                                <label>Short Detail</label>
                                <input type="text" class="form-control" name = "short_detail" id="short_detail"   placeholder="">
                             </div>
                             <input type="hidden" name="employee_image"  id="employee_image" value="xyz.jpeg" />
                            <!-- <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div> -->
                            <button type="submit" class="btn btn-primary">Submit</button>

                            
                        </div>
                        </form>

                        </div>
                    </div>
                    </div>
                <div class="card-header"> DASHBOARD</div>
                
                <div class="row  justify-content-md-center">
            <div class="col-md-6 ">
               <h3>ADD EMPLOYEE DETAILS</h3>
                   <!-- Button trigger modal -->
               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-employee-details">
                    ADD EMPLOYEES
                    </button>
                <a href="{{route('employee-index')}}"> SHOW EMPLOYEE DETAILS</a>

        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    
    $(document).ready(function(){
        $('#addform').on('submit',function(e){

            e.preventDefault();
           
           $.ajax({
               type: "POST",
               url : '/admin/employee-add',
               data: $('#addform').serialize(),
               error: function(xhr, status, error) {
                  
                  var err = eval("(" + xhr.responseText + ")");
                 console.log(err);
                  },
               success: function(response){
                console.log(response)
                $('#add-employee-details').modal('hide')
                alert("Data Saved");
               }
               

           });
        });
    });
</script>
@endsection
