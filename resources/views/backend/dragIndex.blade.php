<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EMPLOYEE HIERARCHY CHECK | EDIT </title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body>
    <div class="container text-center" style="background:#ACB7E8;">
        <h1 style="padding: 30px;font-weight: bolder;">EMPLOYEE HIERARCHY EDITOR</h1>
    </div>
        <div class="big-card" style="background: #DDE5FE;">    
        <div class="row justify-content-center">
        
            <div class="col-md-8 ">
            <table class="table table-hover" style="margin: 10px;border :solid 2px #fff;">
                    <thead>
                        <tr>HIGHEST POSITION</tr>
                    </thead>
                    <tbody>
                        @foreach($employee_data as $value)
                        @if($value->role == 0)
                        <tr data-index="{{$value->id}}" data-position="{{$value->position}}">
                            <td>{{$value->name}}</td>
                            <td>
                            <a href="#" id="{{$value->id}}" class="delbutton" title="Click To Delete">DELETE</a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            <table class="table table-hover"  style="margin: 10px;border :solid 2px #fff;">
                    <thead>
                        <tr>1st Level Employee:</tr>
                    </thead>
                    <tbody>
                        @foreach($employee_data as $value)
                        @if($value->role == 1)
                        <tr data-index="{{$value->id}}" data-position="{{$value->position}}">
                            <td>{{$value->name}}</td>
                            <td>
                            <a href="#" id="{{$value->id}}" class="delbutton" title="Click To Delete">DELETE</a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <table class="table table-hover"  style="margin: 10px;border :solid 2px #fff;">
                    <thead>
                        <tr>2nd Level Employee:</tr>
                    </thead>
                    <tbody>
                        @foreach($employee_data as $value)
                        @if($value->role == 2)
                        <tr data-index="{{$value->id}}" data-position="{{$value->position}}">
                            <td>{{$value->name}}</td>
                            <td>
                            <a href="#" id="{{$value->id}}" class="delbutton" title="Click To Delete">DELETE</a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>

<script type="text/javascript">
    $(document).ready(function() {
        console.log("HERE");
        $('table tbody').sortable({
            update: function(event, ui) {
                $(this).children().each(function(index) {
                    if ($(this).attr('data-position') != (index + 1)) {
                        $(this).attr('data-position', (index + 1)).addClass('updated');
                    }
                });
                saveNewPositions();
            }
        });
    });

    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function() {
            positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
            $(this).removeClass('updated');
        });
        $.ajax({
            
            url: '/admin/test-post',
            method: 'POST',
            dataType: 'text',
            data: {
                _token: "{{ csrf_token() }}",
                update: 1,
                positions: positions
            },
            error: function(xhr, status, error) {
                
                alert(xhr.responseText);
            },
            success: function(response) {
                
                console.log("-SUCCESS-");
            }

        });
    }
   
        $(function() {
        $(document).on('click','.delbutton',function(){
        var element = $(this);
        var del_id = element.attr("id");
        var info = 'id=' + del_id;
        if(confirm("Are you sure you want to delete this Record?")){
           
            $.ajax({
                type: "GET",
                url: "/admin/test-delete",
                data: info,
                error: function(xhr, status, error) {
                
                alert(xhr.responseText);
            },
                success: function(response){  
                    location.reload();
                } 
            });
        }
        return false;
        });
        });

</script>