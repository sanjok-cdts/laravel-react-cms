@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" style="background-color:#D0E8D9;">
                <div class="card-header " style="font-size:20px; font-weight:400; ">ADD MAIL TEMPLATE
                    <form method="post" action="{{route('add-mail-template')}}">

                        <div class="form-group" style="width:40%;">
                            Title: <input type="text" name="title" class="form-control">


                        </div>
                        <div class="form-group">
                            Comment: <textarea name="comment" rows="5" class="form-control" cols="40"></textarea>
                            <br><br>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>


            </div>
        </div>
        <div class="col-md-12" style="margin-top: 20px;">
        <table class="table table-hover" style="background:#FFF7EB" >
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Comment</th>
                
                </tr>
            </thead>
            <tbody>
                @foreach($template_data as $key => $value)
                <tr>
                <th scope="row">1</th>
                <td>{{$value->title}}</td>
                <td>{{$value->comment}}</td>
              
                </tr>
                @endforeach
                
            </tbody>
            </table>
        </div>

    </div>

</div>
@endsection