<html>
   <head>
      <title>Ajax Example</title>
      
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         function getMessage() {
            //  console.log("HERE");
            $.ajax({
               type:'POST',
               url:'/getmsg',
               data: {
                "_token": "{{ csrf_token() }}",
               
                },
               error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
               console.log(err);
                },
               success:function(data) {
                   //console.log(data);
                  $("#msg").html(data.msg);
               }

            });
         }
      </script>
   </head>
   
   <body>
      <div id = 'msg'>This message will be replaced using Ajax. 
         Click the button to replace the message.</div>
      <?php
         echo Form::button('Replace Message',['onClick'=>'getMessage()']);
      ?>
   </body>

</html>