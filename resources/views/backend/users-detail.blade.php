@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">SN</th>
                            <th scope="col">Name</th>
                            <th scope="col">Position</th>
                            <th scope="col">Age</th>
                            <th scope="col">Address</th>
                            <th scope="col"> EDIT | DELETE </th>

                            </tr>
                        </thead>
                        <tbody>
                        @if($user_data)
                        @foreach($user_data as $key => $value)

                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->position }}</td>
                                <td>{{ $value->age }}</td>
                                <td>{{ $value->Address }}</td>
                                <td>
                                <?php $a = $value->id; ?>
                                <a href="{{ route('user-edit',$value->id) }}" class="btn btn-success" style="border-radius:50%"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('user-deleteData',$value->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')" style="border-radius:50%"><i class="fa fa-trash"></i> </a>
                                <a href="{{ url('admin/sendmail', $a)}}" class="btn btn-primary" style="border-radius:3%"><i class="fa fa-envelope-open-o"></i>SEND MAIL </a>
                                
                                    
                                </td>
                            
                            </tr>
                        @endforeach
                        @endif   
                        </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
