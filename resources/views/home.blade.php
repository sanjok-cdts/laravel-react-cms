@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header">Dashboard</div>
               
                <div class="card-body">
                @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>	

                        <strong>{{ $message }}</strong>

                </div>

                @endif
                @if ($message = Session::get('sorry'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>	

                        <strong>{{ $message }}</strong>

                </div>

                @endif

                    You are logged in!<h2> {{ Auth::user()->name }}</h2>
                    @if($user_data)
                        @foreach($user_data as $key => $value)
                        @if($value->name == Auth::user()->name)
                        <table class="table table-striped">
                        
                        <tbody>
                            <tr>
                            <th scope="row">Name : </th>
                            <td>{{ $value->name }}</td>
                            
                            </tr>
                            <tr>
                            <th scope="row">Employee ID : </th>
                            <td>{{ $value->employee_id }}</td>
                            
                            </tr>
                            <tr>
                            <th scope="row">Position :</th>
                            <td>{{ $value->position }}</td>
                            
                            </tr>
                            <tr>
                            <th scope="row">Age : </th>
                            <td>{{ $value->age }}</td>
                            
                            </tr>
                            <tr>
                            <th scope="row">Address : </th>
                            <td>{{ $value->Address }}</td>
                            
                            </tr>

                        </tbody>
                        </table>

                        
                        @endif
                        @endforeach
                        @endif
                    <i class="fa fa-plus" aria-hidden="true"></i><a href="{{ route('fillform')}}">FILL UP THE FORM</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>


// $('#someButton').click(function() {
//     window.location.href = 'http://127.0.0.1:8000/filluptheform';
//     return false;
// });


</script>
